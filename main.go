package main

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

func main() {
	if len(os.Args) < 3 {
		fmt.Println("must specify file to use and cache directory")
		os.Exit(1)
	}
	f, err := ioutil.ReadFile(os.Args[1])
	if err != nil {
		panic(err)
	}
	err = os.MkdirAll(os.Args[2], 0777)
	if err != nil {
		panic(err)
	}
	lines := strings.Split(string(f), "\n")
	for pos, line := range lines {
		fmt.Printf("%s (%d/%d)\n", line, pos, len(lines))
		line = strings.TrimSpace(string(line))
		encoder := sha256.New()
		_, err := encoder.Write([]byte(line))
		if err != nil {
			panic(err)
		}
		cache := hex.EncodeToString(encoder.Sum(nil))
		if _, err := os.Stat(filepath.Join(os.Args[2], cache)); os.IsExist(err) {
			continue
		}
		resp, err := http.Head(strings.TrimSpace(line))
		if err != nil {
			panic(err)
		}
		err = ioutil.WriteFile(filepath.Join(os.Args[2], cache), []byte(strconv.FormatInt(resp.ContentLength, 10)), 0777)
		if err != nil {
			panic(err)
		}
		sleep := rand.Intn(5) + 1
		fmt.Printf("Sleeping for %d seconds...\n", sleep)
		time.Sleep(time.Duration(sleep) * time.Second)
	}
}
